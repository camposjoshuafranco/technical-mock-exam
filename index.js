function countLetter(letter, sentence) {
    let count = 0;

    // Check first whether the letter is a single character.
    // If letter is a single character, count how many times a letter has occurred in a given sentence then return count.
    // If letter is invalid, return undefined.

    let req = letter.length;


    if (req === 1) {

        for(let i = 0; i < sentence.length; i++) {
            if (sentence.charAt(i) === letter) {
                count +=1;
            }
        }

        return count;
    } 
    
}


function isIsogram(text) {
    // An isogram is a word where there are no repeating letters.
    // The function should disregard text casing before doing anything else.
    // If the function finds a repeating letter, return false. Otherwise, return true.

    const upperText = text.toUpperCase()

    const result = upperText.split("").every((c, i) => upperText.indexOf(c) == i)

    return result
    
}

function purchase(age, price) {
    // Return undefined for people aged below 13.
    // Return the discounted price (rounded off) for students aged 13 to 21 and senior citizens. (20% discount)
    // Return the rounded off price for people aged 22 to 64.
    // The returned value should be a string.

    if ((age <= 21 && age >=13) || (age >= 64)) {
        let discount = price - (price*0.2)

        return Number.parseFloat(discount).toFixed(2);
        
    } if ( age >= 22 && age <= 64 ) {
        return Number.parseFloat(price).toFixed(2)
    }
}

function findHotCategories(items) {
    // Find categories that has no more stocks.
    // The hot categories must be unique; no repeating categories.

    // The passed items array from the test are the following:
    // { id: 'tltry001', name: 'soap', stocks: 14, category: 'toiletries' }
    // { id: 'tltry002', name: 'shampoo', stocks: 8, category: 'toiletries' }
    // { id: 'tltry003', name: 'tissues', stocks: 0, category: 'toiletries' }
    // { id: 'gdgt001', name: 'phone', stocks: 0, category: 'gadgets' }
    // { id: 'gdgt002', name: 'monitor', stocks: 0, category: 'gadgets' }

    // The expected output after processing the items array is ['toiletries', 'gadgets'].
    // Only putting return ['toiletries', 'gadgets'] will not be counted as a passing test during manual checking of codes.

    let noStack =  items.filter(function(item) {
        return item.stocks == "0" 
    });

    const array = [...new Map(noStack.map(item => [item.category, item])).values()]

    let result = array.map(function (i) {
        return i.category
    })

    return result

}

function findFlyingVoters(candidateA, candidateB) {
    // Find voters who voted for both candidate A and candidate B.

    // The passed values from the test are the following:
    // candidateA: ['LIWf1l', 'V2hjZH', 'rDmZns', 'PvaRBI', 'i7Xw6C', 'NPhm2m']
    // candidateB: ['kcUtuu', 'LLeUTl', 'r04Zsl', '84EqYo', 'V2hjZH', 'LIWf1l']

    // The expected output after processing the candidates array is ['LIWf1l', 'V2hjZH'].
    // Only putting return ['LIWf1l', 'V2hjZH'] will not be counted as a passing test during manual checking of codes.

    const filtered = candidateA.filter(value => candidateB.includes(value)).filter((value, index, self) => self.indexOf(value) === index);

    return filtered
    
}

module.exports = {
    countLetter,
    isIsogram,
    purchase,
    findHotCategories,
    findFlyingVoters
};